import { jsPDF } from "jspdf";
import { formatDate } from "../formatDate";

export const boxEntryExit = (pdf) => {
  const doc = new jsPDF();

  let pageHeight = doc.internal.pageSize.height;
  let height = 0;
  let page = 1;

  /**
   * Seta informações da empresa - lanchonete
   */
  doc.setDrawColor(0, 0, 0);
  doc.setFont("bold");
  doc.setFontSize(16);

  height += 10;

  doc.addImage(pdf.logo, 'JPEG', 5, 5, 0, 15);

  doc.text(`Empresa: ${pdf.name}`, 100, height, {
    baseline: "top",
    align: "center",
  });

  doc.setFontSize(14);
  doc.text(`CNPJ: ${pdf.cnpj}`, 170, height, {
    baseline: "top",
    align: "center",
  });

  /**
   * Seta informações sobre os caixas
   */
  doc.setFont("normal");

  pdf.boxes.map((box, index, { length }) => {
    if (height >= pageHeight) {
      doc.addPage();
      height = 0;
      page += 1;
    }

    height += 17;
    doc.text(`Caixa: ${box.id}`, 60, height);

    height += 8;
    doc.setFontSize(12);
    doc.text(
      `Valor na Abertura: ${box.in_value === 0 ? "0,00" : box.in_value}`,
      10,
      height
    );
    doc.text(
      `Valor no fechamento: ${
        !box.out_value || box.out_value === 0
          ? "0,00 - (Caixa está aberto!)"
          : box.out_value
      }`,
      55,
      height
    );

    height += 8;
    doc.text(`Data de Abertura: ${formatDate(box.date_open)}`, 10, height);
    doc.text(
      `Data de fechamento: ${
        !box.date_close ? "Em aberto" : formatDate(box.date_close)
      }`,
      65,
      height
    );

    height += 2;
    doc.line(10, height, 200, height);

    height += 5;
    doc.text("Entradas:", 10, height);
    doc.text("Saídas:", 100, height);

    height += 3;
    doc.line(10, height, 200, height);

    height += 5;

    let linesEntry = 0;
    let totalValueEntry = 0;
    let totalValueExit = 0;
    let lineTotal = 0;

    box.boxEntry.map((entry) => {
      linesEntry += 1;
      totalValueEntry += entry.value;

      doc.text("Data:", 12, height);
      doc.text(formatDate(entry.date), 25, height);

      doc.text("Valor:", 55, height);
      doc.text(`${entry.value}`, 70, height);

      height += 3;
      doc.setDrawColor(214, 214, 214);
      doc.line(10, height, 200, height);
      doc.setDrawColor(0, 0, 0);
      height += 8;

      if (linesEntry >= box.boxEntry.length) {
        lineTotal = height;
        doc.text("Total:", 55, height);
        doc.text(`${totalValueEntry}`, 70, height);

        height += 2;
        doc.line(10, height, 200, height);
      }
    });

    height = 53;
    height += 5;
    box.boxExit.map((exit) => {
      totalValueExit += exit.value;

      doc.text("Data:", 102, height);
      doc.text(formatDate(exit.date), 115, height);

      doc.text("Valor:", 155, height);
      doc.text(`${exit.value}`, 170, height);

      height += 8;

      if (linesEntry >= box.boxEntry.length) {
        doc.text("Total:", 155, lineTotal);
        doc.text(`${totalValueExit}`, 170, lineTotal);
      }
    });

    height += 10;

    if (index + 1 === length) {
      let heightPage = pageHeight - height - 7 + height;
      doc.text(`Página: ${page}`, 190, heightPage);
    }
  });

  doc.save(`${pdf.name}-${pdf.cnpj}.pdf`);

  return doc;
};
