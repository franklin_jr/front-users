import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#185D78',
        secondary: '#0C303E',
        accent: '#8c9eff',
        error: '#b71c1c',
        titleText: '#4F4F4F'
      },
    },
  },
});
