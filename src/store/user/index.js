import { userService } from '@/service'

export default {
  state: {
    user: null,
    currentCompany: {name: ''}
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload
    },
    setCurrentCompany(state, payload) {
      state.currentCompany = payload
    },
    UNSET_USER(state) {
      state.user = null;
    },
  },
  actions: {
    destroyUser(_, payload) {
      return new Promise((resolve, reject) => {
        userService.destroy(payload)
          .then(resp => {
            resolve(resp.data)
          }).catch(err => {
            reject(err)
          })
      })
    }
  }, 
  getters: {
    user (state) {
      return state.user
    },
    currentCompany (state) {
      return state.currentCompany
    }
  }
}