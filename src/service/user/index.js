import http from '../api'
import Paginator from '../paginator'

let index = (config) => {
    return new Paginator({
        url: 'users/',
        config: config
    });
}

let update = (id) => {
  return http.put(`users/${id}`)
}

let destroy = (id) => {
  return http.delete(`users/${id}`)
}

export default {
  index,
  update,
  destroy
}