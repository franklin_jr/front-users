// axios
import axios from 'axios';

let baseURL = process.env.VUE_APP_URL;
const instance = axios.create({
  baseURL,
  auth: {
    username: 'admin',
    password: process.env.VUE_APP_SECRET_API_KEY
  }
  //withCredentials: true
});

instance.interceptors.request.use(config => {
  config.headers.post['Content-Type'] = 'application/json'
  return config;
});


export default instance;