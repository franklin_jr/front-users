import Vue from 'vue'
import { format } from 'date-fns';
import { zonedTimeToUtc } from 'date-fns-tz'
const tz = Intl.DateTimeFormat().resolvedOptions().timeZone
Vue.mixin({
  methods: {
    dateFormatter(date, formatt) {
      if(!formatt)
        formatt = 'dd/MM/yyyy HH:mm:ss'
      try {
        return format(zonedTimeToUtc(date, tz), formatt);
      } catch (e) {
        return 'INVALID'
      }
    },
  },
})