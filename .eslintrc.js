module.exports = {
    'extends': [
        'eslint:recommended',
        'plugin:vue/base',
        'plugin:vue/essential'
    ],
    'parserOptions': {
        'parser': 'babel-eslint'
    },
    'env': {
        'browser': true,
        'commonjs': true,
        'node': true,
        'es6': true
    },
    'rules': {
        'quotes': [
            2,
            'single',
            'avoid-escape'
        ]
    }
  }