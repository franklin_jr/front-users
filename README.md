# Dashboard Pharma Inc - Fullstack Challenge 20201209
Front-end application with the purpose of showing a list of users

## Setup

#### Create the `.env` file with the information below. 

```bash
    VUE_APP_URL=${URL_API}
    VUE_APP_SECRET_API_KEY=${API_KEY}
```

### Installation/Build

1) Install required packages:

- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/compose/install/)

2) Clone the repository into your desired directory.

### Running

Building and starting the containers in docker-compose:
```sh
$ cd [your workspace directory]/front-user
$ docker-compose up -d --build
```

Check if  docker container (front-users_web_1) are running:
```sh
$ docker-compose ps 
```

To stop containers and remove images, execute:
```sh
$ docker-compose down -v --rmi 'local'